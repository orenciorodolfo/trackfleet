import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Carousel from './components/Carousel/Carousel'

class App extends Component {
  state = {
    items: [
      {
        text: 'Item 1',
        color: '#ffccaa',
      }, {
        text: 'Item 2',
        color: '#aaccff',
      }, {
        text: 'Item 3',
        color: '#ffccaa',
      }, {
        text: 'Item 4',
        color: '#aaccff',
      }, {
        text: 'Item 6',
        color: '#ffccaa',
      },
    ]
  }

  addItem = () => {
    this.setState({
      items: [
        ...this.state.items,
        {
          text: 'New item',
          color: '#ddd',
        }
      ]
    })
  }

  renderItem = ({ item }) => (
    <View style={styles.item(item.color)}>
      <Text >{item.text}</Text>
    </View>
  )

  render() {
    return (
      <View style={styles.container}>
        <Carousel
          data={this.state.items}
          renderItem={this.renderItem}
          loop={true}
        />
        <TouchableOpacity onPress={this.addItem}>
          <Text>Add item</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
  },
  item: (color) => ({
    height: 100,
    backgroundColor: color,
    justifyContent: 'center',
    alignItems: 'center',
  }),
}


export default App
