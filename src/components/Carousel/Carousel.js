import React, { Component } from 'react'
import { View, Dimensions, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import SnapCarousel from 'react-native-snap-carousel'

class Carousel extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.any).isRequired,
    renderItem: PropTypes.func.isRequired,
    nextIndex: PropTypes.number,
    onIndexChange: PropTypes.func,
    width: PropTypes.number,
  }

  static defaultProps = {
    nextIndex: undefined,
    onIndexChange: () => {},
    width: Dimensions.get('window').width,
  }

  state = {
    index: 0,
  }

  componentWillReceiveProps(nextProps) {
    // Sometimes we'll need to scroll for an amount instead of one by one.
    // Fire the carousel scroll manually when reacting to an nextIndex change.
    if ((typeof nextProps.nextIndex !== 'undefined') && (nextProps.nextIndex !== this.carousel.currentIndex)) {
      this.carousel.snapToItem(nextProps.nextIndex, true)
    }
  }

  prev = () => {
    this.carousel.snapToPrev()
  }

  next = () => {
    this.carousel.snapToNext()
  }

  render() {
    const {
      data,
      renderItem,
      nextIndex,
      onIndexChange,
      width,
      ...carouselProps
    } = this.props

    return (
      <View style={styles.container} >
        <View style={styles.carousel}>
          <SnapCarousel
            ref={(ref) => { this.carousel = ref }}
            data={data}
            renderItem={({ item, index }) => (
              <View style={styles.item(width)}>
                {renderItem({ item, index })}
              </View>
            )}
            itemWidth={width}
            sliderWidth={width}
            inactiveSlideOpacity={1}
            inactiveSlideScale={1}
            onSnapToItem={(index) => {
              onIndexChange(index)
              this.setState({
                index,
              })
            }}
            activeSlideOffset={100}
            {...carouselProps}
          />
        </View>
      </View>
    )
  }
}

const styles = {
  item: width => ({
    flex: 1,
    width,
  }),
  container: {
    flex: 1,
  },
  carousel: {
    flex: 1,
  },
}

export default Carousel
